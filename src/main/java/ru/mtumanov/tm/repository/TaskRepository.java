package ru.mtumanov.tm.repository;

import java.util.List;
import java.util.stream.Collectors;

import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.Task;

public final class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();       
        return models.stream()
                .filter(m -> projectId.equals(m.getProjectId()) && userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

}
