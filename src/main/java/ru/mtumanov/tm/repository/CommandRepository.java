package ru.mtumanov.tm.repository;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.command.AbstractCommand;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    public void add(AbstractCommand abstractCommand) {
        if (abstractCommand == null) return;
        final String name = abstractCommand.getName();
        if (name != null && !name.isEmpty()) mapByName.put(name, abstractCommand);
        final String argument = abstractCommand.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, abstractCommand);
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return mapByName.get(name);
    }

    @Override
    public AbstractCommand getCommandByArgument(String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return mapByArgument.get(arg);
    }

}
