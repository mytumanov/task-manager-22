package ru.mtumanov.tm.repository;

import ru.mtumanov.tm.api.repository.IRepository;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.entity.EntityNotFoundException;
import ru.mtumanov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return models.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public M findOneById(final String id) {
        return models.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public M remove(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) throws AbstractEntityNotFoundException {
        final M model = findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        models.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(final Integer index) throws AbstractEntityNotFoundException {
        final M model = models.get(index);
        if (model == null) throw new EntityNotFoundException();
        models.remove(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public boolean existById(final String id) {
        return findOneById(id) != null;
    }

}
