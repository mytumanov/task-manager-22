package ru.mtumanov.tm.enumerated;

import java.util.Comparator;

import ru.mtumanov.tm.comparator.CreatedComparator;
import ru.mtumanov.tm.comparator.NameComparator;
import ru.mtumanov.tm.comparator.StatusComparator;
import ru.mtumanov.tm.model.Task;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    private final String displayName;

    private final Comparator<Task> comparator;

    TaskSort(final String displayName, final Comparator<Task> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static TaskSort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator<Task> getComparator() {
        return comparator;
    }

}
