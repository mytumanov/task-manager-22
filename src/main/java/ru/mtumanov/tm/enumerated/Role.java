package ru.mtumanov.tm.enumerated;

public enum Role {
    
    USUAL("Ususal user"),
    ADMIN("Administrator");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
