package ru.mtumanov.tm.exception.field;

import ru.mtumanov.tm.exception.AbstractException;

public abstract class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
    }

    public AbstractFieldException(final String message) {
        super(message);
    }

    public AbstractFieldException(final String messgae, final Throwable cause) {
        super(messgae, cause);
    }

    public AbstractFieldException(final Throwable cause) {
        super(cause);
    }

    public AbstractFieldException(
            final String message, final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
