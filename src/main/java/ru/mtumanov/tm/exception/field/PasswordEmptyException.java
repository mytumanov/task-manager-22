package ru.mtumanov.tm.exception.field;

public class PasswordEmptyException extends AbstractFieldException {

    public PasswordEmptyException() {
        super("ERROR! Password is empty!");
    }
    
}
