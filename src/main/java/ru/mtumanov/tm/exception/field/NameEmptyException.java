package ru.mtumanov.tm.exception.field;

public final class NameEmptyException extends AbstractFieldException {

    public NameEmptyException() {
        super("ERROR! Name is empty!");
    }

}
