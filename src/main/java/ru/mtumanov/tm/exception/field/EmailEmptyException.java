package ru.mtumanov.tm.exception.field;

public class EmailEmptyException extends AbstractFieldException {
    
    public EmailEmptyException() {
        super("ERROR! Email is empty!");
    }
    
}
