package ru.mtumanov.tm.exception.entity;

public class EntityNotFoundException extends AbstractEntityNotFoundException {

    public EntityNotFoundException() {
        super("ERROR! Entity not found!");
    }

    public EntityNotFoundException(String entity) {
        super("ERROR! Entity:" + entity + " not found!");
    }

}
