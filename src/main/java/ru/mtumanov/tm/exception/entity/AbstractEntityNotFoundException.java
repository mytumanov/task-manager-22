package ru.mtumanov.tm.exception.entity;

import ru.mtumanov.tm.exception.AbstractException;

public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException() {
    }

    public AbstractEntityNotFoundException(final String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(final String messgae, final Throwable cause) {
        super(messgae, cause);
    }

    public AbstractEntityNotFoundException(final Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundException(
            final String message, final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
