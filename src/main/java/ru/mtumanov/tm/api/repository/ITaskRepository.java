package ru.mtumanov.tm.api.repository;

import java.util.List;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Task;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId) throws AbstractException;

}
