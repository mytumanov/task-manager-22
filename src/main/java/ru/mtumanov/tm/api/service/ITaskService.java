package ru.mtumanov.tm.api.service;

import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    List<Task> findAllByProjectId(String userId, String projectId) throws AbstractException;

    Task create(String userId, String name, String description) throws AbstractException;

    Task updateById(String userId, String id, String name, String description) throws AbstractException;

    Task updateByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Task changeTaskStatusByIndex(String userId, Integer index, Status status) throws AbstractException;

    Task changeTaskStatusById(String userId, String id, Status status) throws AbstractException;

}
