package ru.mtumanov.tm.command.user;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "update current user profile";
    }

    @Override
    public String getName() {
        return "update-user-profile";
    }

    @Override
    public void execute() throws AbstractException {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().userUpdate(userId, firstName, lastName, middleName);
    }
    
}
