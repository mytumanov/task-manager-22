package ru.mtumanov.tm.command.user;

import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "lock user";
    }

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        getServiceLocator().getUserService().lockUserByLogin(login);
    }
    
}
