package ru.mtumanov.tm.command.user;

import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class UserRemoveCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "user remove";
    }

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        getServiceLocator().getUserService().removeByLogin(login);
    }
    
}
