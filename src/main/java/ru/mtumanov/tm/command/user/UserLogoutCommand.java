package ru.mtumanov.tm.command.user;

import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "logout current user";
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();        
    }
    
}
