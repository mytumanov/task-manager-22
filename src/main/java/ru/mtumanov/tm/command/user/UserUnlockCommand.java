package ru.mtumanov.tm.command.user;

import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "unlock user";
    }

    @Override
    public String getName() {
        return "user-unlock";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        getServiceLocator().getUserService().unlockUserByLogin(login);
    }
    
}
