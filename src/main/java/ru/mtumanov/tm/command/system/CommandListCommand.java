package ru.mtumanov.tm.command.system;

import java.util.Collection;

import ru.mtumanov.tm.command.AbstractCommand;

public class CommandListCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Show command list";
    }

    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public void execute() {
        System.out.println("[Commands]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }
    
}
