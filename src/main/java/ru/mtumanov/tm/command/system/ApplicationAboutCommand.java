package ru.mtumanov.tm.command.system;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show about program";
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Maksim Tumanov");
        System.out.println("e-mail: mytumanov@t1-consulting.ru");
        System.out.println("e-mail: MYTumanov@yandex.ru");
    }
    
}
