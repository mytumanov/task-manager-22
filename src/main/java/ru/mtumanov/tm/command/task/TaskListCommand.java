package ru.mtumanov.tm.command.task;

import java.util.Arrays;
import java.util.List;

import ru.mtumanov.tm.enumerated.TaskSort;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Show list tasks";
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final TaskSort sort = TaskSort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, sort.getComparator());
        renderTask(tasks);
    }
    
}
