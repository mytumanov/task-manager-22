package ru.mtumanov.tm.command.task;

import ru.mtumanov.tm.exception.AbstractException;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK CLEAR]");
        final String userId = getUserId();
        getTaskService().clear(userId);
    }
    
}
