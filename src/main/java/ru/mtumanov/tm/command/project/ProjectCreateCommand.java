package ru.mtumanov.tm.command.project;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }
    
}
