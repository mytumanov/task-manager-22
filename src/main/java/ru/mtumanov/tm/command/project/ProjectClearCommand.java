package ru.mtumanov.tm.command.project;

import ru.mtumanov.tm.exception.AbstractException;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECTS CLEAR]");
        final String userId = getUserId();
        getProjectService().clear(userId);
    }
    
}
